function setup() {
  // put setup code here
  createCanvas(500, 500);
  background(80,120,255);
}

function draw() {
  // put drawing code here
  //Stor blomst 
  noStroke();
  fill(205,22,92);
  ellipse(250, 150 ,100, 100);
  noStroke();
  ellipse(180, 180 ,100, 100);
  noStroke();
  ellipse(320, 180 ,100, 100);
  noStroke();
  ellipse(190, 250 ,100, 100);
  noStroke();
  ellipse(310, 250 ,100, 100);
  noStroke();
  ellipse(250, 280 ,100, 100);
  fill(0,0,0);
  ellipse(250, 215, 80, 80);
  fill(34, 139, 34);
  rect(235, 328, 30, 175);
  //lille blomst rød
  rect(105, 435, 10, 75);
  fill(255, 69, 0);
  rect(82, 400, 55, 55, 20);
  fill(0,0,0);
  ellipse(109, 428, 20, 20);
  //lille blomst orange
  fill(34,139,34);
  rect(355, 435, 10, 75);
  fill(255, 140, 0);
  rect(332, 400, 55, 55, 20);
  fill(0,0,0);
  ellipse(359, 428, 20, 20);
  //Lille blomst lilla
  fill(34,139,34);
  rect(430, 465, 10, 75);
  fill(148, 0, 211);
  rect(415, 430, 45, 45, 20);
  fill(0,0,0);
  ellipse(438, 453, 15, 15);
  //lille blomst hvid
  fill(34,139,34);
  rect(180, 465, 10, 75);
  fill(255, 255, 255);
  rect(165, 430, 45, 45, 20);
  fill(0,0,0);
  ellipse(188, 453, 15, 15);
  //lille blomst turkis
  fill(34,139,34);
  rect(40, 465, 10, 75);
  fill(0, 255, 255);
  rect(23, 430, 45, 45, 20);
  fill(0,0,0);
  ellipse(46, 453, 15, 15);
}
