## Hvad har jeg produceret? 
Jeg har lavet en blomsterpark. Jeg fik inspiriation fra en veninde, der elkser blomster. Jeg tænkte derfor det kunne være sjovt at se om jeg kunne finde ud af at programmere nogle forskellige blomster. 

For at lave min blomsterpark har jeg brugt Visual Studio Code. Jeg startede med at oprette et kanvas, hvilket jeg gjorde ved at skrive creatCanvas(500, 500) og derefter background(80,125,255) for at lave baggrunden en blå himmel farve, så det passer til en udendørs blomsterpark. Dernæst ville jeg lave den store blomst, og der har jeg gjort ved hjælp af ellipse() og så lavede jeg 6 lige store ellipser og placerede dem rundt i en cirkel, for af kreare det der ligner de forsekllige blade. Til disse ellipse har jeg også benyttet mig af noStroke(), for at man ikke kan se de individuelle cirker. Dernæst lavede jeg den sorte center cirkel i midten af blomsten, og dette gjorde jeg osgå ved hjælp af ellipse(). Sidst til den store blomst tilføjede jeg en stilk, hvilket jeg gjorde ved at benytte rect() og placerede den under blomsten. 

Her er noget udklip af koden til den store blomst: 

noStroke();

ellipse(250, 280 ,100, 100);

fill(0,0,0);

ellipse(250, 215, 80, 80);

fill(34, 139, 34);

rect(235, 328, 30, 175);

Jeg ville gerne lave nogle flere små blomster, for at gøre det mere til en mini blomsterpark end bare en stor blomst. Disse ville jeg lave lidt mere enkle, så jeg gjorde det ved at lave dem af afrundede firkanter, hvilket jeg brugte rect() til at gøre. Til disse blomster lavede jeg også en midte, ved hjælp af ellipse() og stilke ved hjælp af rect(). Jeg lavede disse små blomster i forskellige størrelser og farver, og placerede dem rundt omkring den store blomst. 
Både den store blomst og de små blomster har jeg givet en masse forskeliige farver, og dette har jeg gjort ved at benytte fill() og give de forsekllige ellipser og firkanter farver. 

Alle de forskellige former har jeg fået fra p5.js reference listen https://p5js.org/reference/ 

[Se programmeringen her](https://signeam.gitlab.io/aestetisk-programmering/Minix1/index.html)

## Hvordan vil jeg beskrive min første selvstændige oplevelse?
Det har været sjovt at komme i gang med at programmere. Jeg synes det er fedt den måde man meget tydeligt får noget konkret ud af det man sidder og laver. Jeg synes det er mere motiverende og spændende at lave noget når mans å tydeligt kan se hvad vi får ud af det vi læser og lærer. 

Dog har det også været en til tider lidt frustrende process, da det på nogle punkter har været svært at få at til at fungere som det skulle. Både selve programmeringsopgaven, med at få de rigtge størrelser på formerne, men især at få placeret dem det rigtige sted på kanvaset har været en smule svært nogle af gangene. Derudover har det været svært for mig at få gitlab til at fungere 100% optimalt. Det har i hvert fald taget lidt tid at finde ud af hvordan jeg skulle lave links osv. herinde. Det har især været svært i forhold til html index filen, da det er meget vigtigt at alt er stavet og refereret korrekt for at det kan virke, og det har til tider været svært at finde ud af hvad der har været galt hvis noget ikke har fungeret. 

Det har dog generelt været fedt at komme i gang med at programmere.

## Hvordan er kodningsprocessen forskellig fra, eller minder om, læsning og skrivning? 
I forhold til at finde ud af hvilke syntakser man kan skal bruge altså hvilke former, farver osv, man skal bruge for at lave det man gerne ville, minder det meget om at læse alt mulig andet synes jeg. Da man skal læse hvad fx. rect() gør, hvad man bruger den til, hvilke tal man skal bruge til at få den rigtige form, størrelse og placering man gerne vil have. Man skal læse og forstå det, for at kunne bruge det, lidt ligesom man ville gære med alt mulig andet faglig læsning. Så her ville jeg sige det minder om andet akedemisk læsning, som man skal forstå før man kan benytte sig af det. 

## Hvad betyder kode og programmering for dig? 
Det synes jeger lidt svært at svare på. Jeg ved at stort set alt it, hjemmesider apps osv. er der kodning i for at det fungere. Så jeg ved at jeg benytter mig at teknologi der er programmeret og kodning i hver eneste dag. Det er dog ikke noget jeg på nuværende tidspunkt tænker over når jeg benytter det. Jeg er dog sikker på at jo mere viden jeg får omkring programmering, jo mere vil jeg sikkert også tænke over hvad der egentlig indgår og får de forskellige hjemmesider og apps mv. som jeg bruger hver dag til at fungere som de gør. 
