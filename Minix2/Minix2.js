function setup() {
  // put setup code here
  createCanvas(windowWidth, windowHeight);
  background(255,140,50);
  frameRate(3);}
  
 
function draw() {
  // put drawing code here
  //når musen køre henover emojien, skifter den til denne emoji 
 if(mouseX > 250 && mouseX < 500 && mouseY > 250 && mouseY < 500){
  //Den emoji der kommer når man har musen henover
  
  fill(random(100,200),random(100,200),random(100,200));
  rect(250,250,200,200, 70);
  strokeWeight(5);

  //venstre øje
  fill(0,0,0);
  ellipse(310,320,35,35);
  fill(255,255,255)
  ellipse(310,320,20,20);

  //højre øje 
  fill(0,0,0);
  ellipse(390,320,35,35);
  fill(255,255,255)
  ellipse(390,320,20,20);

  //mund
  fill(random(100,200), random(100,200), random(100,200));
  arc(350,390,80,80,0,PI,CHORD);
 
 //min standart emoji 
 } else {
   
  //bagrunden på emojien 
  fill(66,193,78);
   rect(250,250,200,200,70);
   strokeWeight(5);

   //venstre øje
   fill(0,0,0);
   ellipse(310,320,35,35);
   fill(255,255,255)
   ellipse(310,320,20,20);

   //højre øje 
   fill(0,0,0);
   ellipse(390,320,35,35);
   fill(255,255,255)
   ellipse(390,320,20,20);

   //mund
   fill(23,131,33);
   arc(350,390,80,80,0,PI,CHORD);
  }

   
 
  // put drawing code here
  //når musen køre henover emojien, skifter den til denne emoji 
 if(mouseX > 550 && mouseX < 750 && mouseY > 250 && mouseY < 450){
  //Den emoji der kommer når man har musen henover
  fill(random(100,200),random(100,200),random(100,200));
  rect(550,250,200,200, 70);
  strokeWeight(5);

  //venstre øje
  fill(0,0,0);
  ellipse(610,320,35,35);
  fill(255,255,255)
  ellipse(610,320,20,20);

  //højre øje 
  fill(0,0,0);
  ellipse(690,320,35,35);
  fill(255,255,255)
  ellipse(690,320,20,20);

  //mund
  fill(random(100,200), random(100,200), random(100,200));
  arc(650,420,80,80,PI,0,CHORD);
 //min standart emoji 
 } else {
   
  //bagrunden på emojien 
  fill(102,0,204);
   rect(550,250,200,200,70);
   strokeWeight(5);

   //venstre øje
   fill(0,0,0);
   ellipse(610,320,35,35);
   fill(255,255,255)
   ellipse(610,320,20,20);

   //højre øje 
   fill(0,0,0);
   ellipse(690,320,35,35);
   fill(255,255,255)
   ellipse(690,320,20,20);

   //mund
   fill(51,0,102);
   arc(650,420,80,80,PI,0,CHORD);
  } 

}
