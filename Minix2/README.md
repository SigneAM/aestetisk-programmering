# Mini x 2 - Emojis

![](Emojis.png)


## beskriv dit program, hvad du har brugt og hvad du har lært?
Jeg har lavet to smileyer i en lidt utraditionel form , da der er firkantede, den ene smiler og er glad, og den anden er sur. De har begge to til at starte med det jeg ville kalde for neutrale farver, men når du så kører musen henover dem skifter de til en random farve. 

[Se programmeringen her](https://SigneAM.gitlab.io/aestetisk-programmering/Minix2/index.html)

[Se koden her](https://gitlab.com/SigneAM/aestetisk-programmering/-/blob/main/Minix2/Minix2.js)

![](emojivideo.gif)

Jeg startede med at lave mine to basis figure ved hjælp af `rect()` til formen `ellipse()` til øjene og `arc()` til munden. 
###### Nedenstående kode er gældenen for begge emojis, tager bare udegangspunkt i en af dem 
Dernæst for at få dem til at skifte farve brugte jeg et `if()` statement 
``` 
 if(mouseX > 250 && mouseX < 500 && mouseY > 250 && mouseY < 500){
  fill(random(100,200),random(100,200),random(100,200));
  rect(250,250,200,200, 70);
```
Når musen er henover emojien/ de ovenstående kordinater, så vil den skifte til en random farve. 


Jeg havde også en `else()` statement, til når musen ikke er henover emojien, så ville den se ud på en bestemt måde - altså den måde den ser ud lige når man kommer ind på den uden man har musen henover. Til det ser koden således ud 
```
 } else {
 fill(66,193,78);
 rect(250,250,200,200,70);
```
Jeg har i denne Mini x 2 benyttet mig af `arc()`, og `if()`og `else()` statements/funktioner som er nye for mig. Jeg har dertil også lært hvordan man benytter sig af random farver, og hvordan man får den til at skifte farve når man ønsker det, altså min if statement. Jeg lærte også hvordan jeg laver `arc()` til en halvcirkel, og hvordan jeg fik den til at vende den vej jeg gerne ville have dn til. Dernæst lærte jeg hvad `if() & else()`statements betyder, og hvordan jeg bruger funktionen til at ændre farve, når jeg har musen inden for nogle bestemte kordinater, og lærte hvordan jeg finder frem til de bestemte kordinater. Netop dette havde jeg en smule problemer med i starten, da jeg lige ksal instille min hjerne til at bruge noget metaematik igen, i forhol til at regen ud hvor mine x og y kordinater er. 
Så der har været nogle ting i min mini x 2 der har været lidt svære for mig, men tilgengæld har jeg nu også lært hvordan man benytter sig at nogle nye funktioner jeg ikke kendte til før. 

## Hvordan ville du sætte din emoji ind i en bredere social og kulturel kontekst, der vedrører en repræsentationspolitik, identitet, race, kolonialisme og så videre?
Jeg har lavet disse to emojis i nogle anderledes former og farver for at det ikke skal repræsentere et menenske som de ellers tit bliver asocieret med. Derudover har jeg gjort så de kan skifte farver, så man hellere ikke aktivt skal vælge hvilken farve man gene vil have den til at være. 

Jeg synes det var lidt svært lige til at starte med at finde ud af at lave emojis som på en eller andet måde skulle kommentere på hvordan samfundet er, eller altså skulle sættes i en social kontekst og/eller skulle kommentere på noget politisk. Jeg synes det er spænende at kigge på hvordan det kan påvirke hvordan vi ser verdenen ved de emojis vi bruger, jeg synes dog osgå det kan være svært at se hvordan det er en vigtig samtale, da jeg mener der er andre emner der er vigtigere end dette. Men på samme tid kan jeg godt se hvordan det kan have relevans, fordi man bruger og eller ser emojis så meget i sin hverdag, at det måske underbevidst påvirker en, eller man i hvert fald underbevidst opfatter hvilke emojis der bliver valgt, og det derfor er vigtigt at der er et bredt udvalg af emojis så alle kan føle sig inkluderet. 

klassen snakkede vi om emojis og snakkede om hvordan emojis kan have betydning for hvordan samfundet er. Altså om hvordan emojis og de vlag vi har af emojis mulgivis kan udelukkede ngole minioriteter eller genelret folk der mener de ikke er repræsentative nok. I denne snak, snakkede vi om hvordan man nu kan vælge farve på sine emojis, der er 6 firskellige farver, forsekllige nuancer af "hudfarver". her sad jeg dog og tænkte jeg på, hvordan det er godt at der er et valg og man dermed kan vælge den farve de ræsonnere bedst med. Dog synes jeg også at det at man skal vælge en farve gør at man ikke kan neutral med det, og kan dermed også skabe nogle problemer. For hvorfor sakl man tage et valg, og ved at man gør det kommer man så til at udelukke nogle? 

Hele denne samtale og de tanker den satte i gang i mig gjorde at jeg gerne ville finde en måde at lave en neutral emoji i hvert fald i forhold til race og farve, og det var derfor jeg gerne ville lave emojis der kunne skifte til en random farve når man trykekde på den, så man ikke selv skulle tage stilling til hvilken farve man valgte, og dermed ikke udelukkede nogle. 



#### Referenceliste 
https://p5js.org/reference/ 




