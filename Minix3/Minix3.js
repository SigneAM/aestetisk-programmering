//throbber
function setup() {
  //create a drawing canvas
  createCanvas(windowWidth, windowHeight);
  frameRate (30);  
 }


 function draw() {
   background(140,235,255);  
   
   //Banen bilen kører på 
   push()
   translate(width/2, height/2);
   fill(100);
   ellipse(0,0,320,320);
   fill(255);
   ellipse(0,0,240,240);
   fill(100);
   ellipse(0,0,235,235);
   fill(140,235,255);
   ellipse(0,0,150,150);
   drawElements();
   pop()

   // skrift på bagrunden 
   push()
   textFont('courier New');
   textSize(50);
   text('Hvem kommer først til hjemmesiden?', 200, 80);
   textSize(30)
   text('Dig eller Mcqueen?', 550, 140);
   pop()
 }


 function drawElements() {
   let num = 50;
   push();
   
   let cir = 360/num*(frameCount%num);
   rotate(radians(cir));
   noStroke();
   
   //hjul 
   fill(0);
   ellipse(91, 40, 8, 8);
   ellipse(124, 40, 8, 8);
   ellipse(91, 15, 8, 8);
   ellipse(124, 15, 8, 8);

   //bilen 
   fill(255, 0, 0);
   rect(90, 0, 35, 55, 5, 5, 15, 15);

   //bilens rude
   fill(130);
   rect(95, 30, 25, 20, 5, 5, 15, 15);
   
   //skrift på bilen 
   fill(255,255,0);
   text('95', 100, 20);
   pop();
   
 }
 
 function windowResized(){
  resizeCanvas(windowWidth, windowHeight);
 }
  
