# Mini x 3 - Throbber
![](bil.png)

[se koden her](https://gitlab.com/SigneAM/aestetisk-programmering/-/blob/main/Minix3/Minix3.js)

[se programmeringen her](https://SigneAM.gitlab.io/aestetisk-programmering/Minix3/index.html)

**What do you want to explore and/or express?**

Jeg vil gerne vise hvordan en throbber fungere. jeg synes det var svært at lave noget der havde en større betydning, så jeg valgte is stedet at lave en der kunne være lidt sjov at se på. Det er derfor jeg har lavet en bil/lynet mcqueen, for at henvise til noget de fleste kan genkende fra baggrunden, og for at spille på det med at han er hurtigt, og det er meningen at en throbber skal være hurtigt væk, for at symbolisere at man hurtigt kommer ind på den hjemmeside man gerne vil ind på. 

**What are the time-related syntaxes/functions that you have used in your program, and why have you used them in this way?**

I min kode har jeg i forhold til tid benyttet mig af `frameRate()` for at få bilen til at køre rundt i den hastighed jeg gerne ville have. Jeg har valgt `frameRate(30)` for at få den til at køre så hurtigt at man kan fornemme det med at det skal gå hurtigt fordi man geen vil hurtigt ind på hjemmeside, men langsomt nok til at man også kan se hvad det er der kører rundt, altså bilen her. 

Det var vigtigt for mig at man kunne se hvad det var der kørte rundt for at det var "sjovere" at se på min throbber i forhold til andre man måske kender fra nettet af, men samtidig var det også vigtigt fpr mig at den havde en bestemt hastighed. Det med hastigheden var vigtigt for mig fordi man underbevidst forbinder hvor hurtigt throbberen kører, med hvor hurtigt computeren arbejder inde bagved / altså hvor hurtigt man når frem til hjememsiden. 

Selve den kode jeg har brugt for at få bilen til at køre rundt i en cirkel, ser sådan her ud:
``` 
let num = 50;
   push();
   
   let cir = 360/num*(frameCount%num);
   rotate(radians(cir));
```
Min let num = indikere hvor hurtigt bilen skal køre rundt, altså jo lavere tallet er, jo hurtigere ville bilen køre rundt. 

Derudover har jeg også lavet en variable der hedder let cir = 360/num*(frameCount%num) som indikere hvor bilen skal kører rundt i en cirkel på framecounten. 

Det har været spændende at arbejde med disse syntakser og variable. Det er nyt for mig at benytte mig af variable og disse syntakser, så jeg skulel lige finde ud af hvordan jeg fik det hele til at virke som jeg gerne ville have det, men da jeg først fandt ud det virkede det lige som jeg ville have det til. 

**Think about a throbber that you have encounted in digital culture, e.g. for streaming video on YouTube or loading the latest feeds on Facebook, or waiting for a payment transaction, and consider what a throbber communicates, and/or hides?**

Jeg forbinder throbbere med at et program/internettet "tænker" og er ved at finde derhen hvor jeg har bedt den om at finde hen, eller loade noget jeg har bedt den om at loade. Så jeg tænker den gemmer processen bag at loade noget, så man ikke ser hvad der foregår bag kulissen, men man ser på den her throbber i stedte for, fordi vi alligevel ikke helt ved hvad det betyder hvis vi så hvad der egentlig foregik inde bag throbberen. Så jeg tror underbevidst at vi alle ved hvad en throbber "gør" eller hvorfor vi ser den, men vi tænker ikke rigtig over hvad der foregår bagved. 
Jeg synes det er godt at vi har throbbere fordi jeg tror vi som menensker er meget utålmodige, især når det kommer til internettet og teknologi, så det er vigtigt for os at se at der sker noget, altså at man kan se throbberen når en hjememside, eller video eller andet loader. Jeg tror at hvis der ikke var noget der indikerede at computeren eller hjememsiden tænkte/loadede, så ville man ikke tro på der skete noget og meget hurtigt lukke det ned, fordi man ikke har meget tålmodighed til at vente på noget, hvis man ikke kan se der sker noget. 
