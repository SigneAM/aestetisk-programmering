# Mini x 4 - CAPTURE ALL / DATA CAPTURE

Start siden af prgrammeringen ser sådan her ud:
![](cookies.png)

Når du trykker enten ja eller nej, ser det således ud. Og der popper forskellige navne, cpr numre, mobil numre, og ip adresser op, og ser sådan her ud: 
![](cookie.png)


[Se programmeringen her](https://SigneAM.gitlab.io/aestetisk-programmering/Minix4/index.html)

[Se koden her](https://gitlab.com/SigneAM/aestetisk-programmering/-/blob/main/Minix4/Minix4.js)

### Titel og beskrivelse 
##### Titel: Data er over det hele
Jeg har lavet denne kodning, som skal vise hvordan at man efterlader digitale fodspor overalt, og hvordan at hvis folk virkelig vil kan de stort se altid finde en måde at skaffe dine data på. Jeg har valgt at skrive/tage udgangspunkt i cookies, fordi det kender stort set alle til, i hvert fald alle der benytter nettet er støttet på en pop-up besked omkring om de vil acceptere cookies. Og sevom jeg godt ved at man kan svare nej, tror jeg at rigtig mange svarer "accepter" fordi man ikek rigtigt tænker over det eller gider sætte sig ind i hvad det egentlig betyder, og hvor meget da de egentlig kan få ud af dig. Denne kdoning er ikke lavet for nødvendigvis at kommentere kun på cookies, men bare geenrelt hvor meget data man egentlig giver væk, ved bare at benytte nettet. Den er på en måde lavet lidt for at skræmme, nok mere bare for at oplyse. For der er ikke nødvendigvis noget farligt i at ens oplysninger ligger et sted på en server, da det på mange punkter hjælper hjemmesider med fx at forbedre ens oplevelser på siden, men synes dog stadig det er vigtigt at kommentere på og vide at det hele ligger derude et sted, og det er muligt at få fat på det på mange måder, og det kan være en halvfarlig ting på nogle punkter 

### Beskrivelse af programmet 
Jeg har i denne kode for første gang benyttet mig af at lave `buttons`, det har jeg gjort ved først at, definere dem ved at skrive `let button1` og `let button2`
og derefter har jeg krearet dem i mit `function setup()`
```
button1 = createButton('JA');
 button1.position(width/3,height/1.7);
 button1.size(100,60);
 button1.mousePressed(sceneskift);

 button2 = createButton('NEJ');
 button2.position(width/1.7,height/1.7);
 button2.size(100,60);
 button2.mousePressed(sceneskift);
 ```
Når man trykker på disse knapper kommer man så videre til min næste skærm/scene, hvor man ser der hvor der står "Ligemeget hvad du svarer, vil vi altid kunne få fat på dine data" og hvor der popper navne, numre, ip adresser og cpr numre op. 

Alle disse data med navnene osv, har jeg også defineret ved at `let data =` og så har jeg skrevet alle navnene osv ind der. 

Alle disse data får jeg så til at poppe op ved at benytte dette `if statement`: 
```
function draw() {
 if (scene == 0){
 textFont('courier New');
 textSize(50);
 text('Vil du acceptere cookies?', width/4, height/2);
 }

 else if (scene==1){
  background(100,100,100);

  let vælg = floor(random(0,85))
push();
  textSize(40);
  strokeWeight(20);
  text(data[vælg],random(0,1500), random(0,850));
```
Det er så mit `else if` der er skærmen hvor dataen dukker op. 


### Kulturelle implikatiner af data capture
Jeg synes det er svært at skrive om data, data capture og capture all, for selvom jeg godt ved hvad data er, så føler jeg bare der er så meegt om det jeg ikke ved. 
Dog synes jeg at det er et vigtigt emne at lære noget om, da jeg tror mange inklusiv mig selv ikke har styr på hvor meget af mit daat der egentlig ligger og flyder rundt der ude et sted i cyberspace, og hvem der egentlig har hvilken data og hvad de egebtlig kan bruge det til. Men jeg tror mange ville have godt at lære lidt mere om det, og lære om hvad ens data bliver brugt til, hvor meget data man egenlgi giver ud til forskellige hjememsider, og hvordan at internettet altid husker, så man skal være klar over hvilke digitale fodspor man efterlader sig. 
