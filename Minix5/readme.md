# mini x 5 - generative program 

![](rød.png)
![](gul.png)

[Se programmeringen her](https://SigneAM.gitlab.io/aestetisk-programmering/Minix5/index.html)

[Se koden her](https://gitlab.com/SigneAM/aestetisk-programmering/-/blob/main/Minix5/Minix5.js)

#### What are the rules in your generative program? Describe how your program performs over time? How do the rules produce emergent behavior?
Jeg synes det var lidt svært at finde ud af hvad jeg skulle lave. Men mit program kører på framerate og laver forskellige former i nogle forskellige linjer, og skifter derudover også baggrundsfarve. 
Jeg har gjort sådan at der bliver genereret ellipser vandret på to parralle linjer, og så bliver der genereret aflange ellipser i forskellige tykkeler på to horinsotalt parralle linjer. 


Jeg har valgt at lave et `if statement` for at få programmet til at køre, og det ser således ud:
``` 
if (frameCount %10 == 0) {
    background(255, random(255),random(255));
   }
```
Det mit `if statement` betyder, er at når framecount går op med 10% så skifter bagrunden til en anden farve. 

Derudover har jeg benyttet mig af et forloop, for at få de forskellige ellipser til at blive genereret, og mit forloop ser således ud: 

``` 
for(let i = 0; i <5; i++){
    fill(255,random(100,200),random(255));
    rect(300, random(height), random(10,50),75, 25);
    ellipse(random(width), 200, 30, 30,);
    rect(500, random(height), 20,75, 25);
    ellipse(random(width), 80, 30, 30,);
    rect(300, random(height), 20,75, 25);
```
Og det beskriver bare hvilke former der skal genereres og hvor. 

Over tid vil programmet se nogenlunde ens ud, da det er de samme former og i samme farveområde (eller hvad man skal kalde det) som farver er i. 
Men fordi det er random placering på lijen, og random farver både for bagrunden og ellipserne, vil programmet hele tiden ændre sig, over tid. Man kan lade programmet køre så lang tid man vil, og programmet vil hele tiden blive ved med at ændre sig. 

#### What role do rules and processes have in your work?
Jeg har egenligt ikke så mange regler i mit program, egentlig kun to, nemlig `if statementet` og forloopen, men disse to relativt simple regler udgør alligvel hele konceptet for min generative opgave, for uden disse to regler ville programmet ikke kunne køre af sig selv, og generere sig gang på gang på gang. Ved at jeg ikek har så mange regler, gør det dem også super vigtige for at mit pogtam kører som det skal. 

#### How does this MiniX help you to understand the idea of “auto-generator” (e.g. levels of control, autonomy, love and care via rules)? 
Ved at sidde og lave denne opgave, har det fåe t mig til at tænke på hvor vigtigt det er at angive de rigtige regler for at få det ud af det som man gerne vil have. Derudoevr tænker jeg også over hvor lidt der egentlig skal til, bare en regel eller to, for at man kan få noget til generere sig selv gang på gang. Det er stadig på en måde et lidt underligt koncept for mig, at der skal så lidt til for at programmmet bare virker og kan køre helt af sig selv, og det får mig til at tænke over hvor mange ting der fungere sådan i dagligdagen.
FX kommer jeg lidt til at tænke på throbbers igen, i forhold til auto-generativ, selvom jeg ikke 100% ved om man kan komme det ind i den her kategori, kommer jeg bare til at tænke på, hvordan det fungere på nettet, når du ser en throbber, så er den ligeosm auto-generereret for at se ud som den gør indtil man kommer videre til den hjememside man gerne vil ind på. 

Hvis jeg tænker på det i forhold til generative art, som vi også har snakket om i forhold til det emne, synes jeg at det også er interreansant hvor meget forskellligt man kan få ud af et program med enkelte regler, og hvor interresant det kan være. Men også hvordan det hurtigt kan blive det samme, så at det måske ikek er så interesant mere efter lidt tid. Fx. med min egen popgave, så er den måske interresant at se på et par sekunder, men når du har sen generere ellipser på forsskellige baggrundsfarver nogle sekunder er det ikke så interresant længere. 

Så synes generelt det er spænedne at tænke på hvor meget forskleligt man kan få ud af nogle få regler i en genereativ opagve, men også hvordan at de samme regler i en opagev hurtigt gør det meget ensformigt. 
