class Data {
  constructor(_text) {
    this.posX = random(0, width);
    this.posY = random(0, height);
    this.speed = random (5, 30);
    this.text = _text;
  }

  move() {
    this.posY += this.speed;
    if (this.posY > height) {
      this.posY = 0;
    }
  }

  show() {
    let vælg = floor(random(0, 85))
    push();
    fill(68, 184, 33);
    textSize(40);
    strokeWeight(20);
    text(this.text,this.posX, this.posY);
    pop();
  }



}



