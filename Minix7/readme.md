# Mini x 7 - Genbesøg af mini x 4 

![](datastart.png)

![](ja.png)

![](nej.png)


[Se min programmering her](https://SigneAM.gitlab.io/aestetisk-programmering/Minix7/index.html)

[Se min kode her til data class](https://gitlab.com/SigneAM/aestetisk-programmering/-/blob/main/Minix7/data.js)

[Se min kode til minix7](https://gitlab.com/SigneAM/aestetisk-programmering/-/blob/main/Minix7/Minix7.js)


### Hvilken miniX har du valgt?
Jeg har valgt at lave om på min mini x 4 om capture all/data capture om. Jeg lavet den om fordi jeg synes at jeg ikke fik det præcise ud af den som jeg gerne ville. Derudover fik jeg noget godt feedback på denne, som gav mig en ide til hvad jeg evt kunne gøre bedre, og det har jeg blandt andet prøvet at gøre.  

### Hvilke ændringer har du lavet og hvorfor?
Jeg har først og fremmest sat en anden baggrund ind på den første side man kommer ind på , for at vise at det skal forestille en hjememside man kommer ind på, hvor man bliver spurgt om cookies. 
Før spurgte den om det samme, altså om man ville acceptere, hvorefter man kunne svarer ja eller nej, men ville komme ind på den samme side efterfølgende ligemeget hvad man svarer. Nu kommer man ind på noget andet hvis man svarer ja, for at indikere at man giver computeren/hjememsiden lov til at få nogle af ens oplysninger når man acceptere cookies. Jeg har nu gjort sådan at når du svarer ja kommer du ikke på en side hvor der stadig bliver vidst en masse foreskellige oplysninger såsom navne, nummre og ip adreser, men nu er der flere om de kommer faldene ned, og derudover starter et kamaera op, som filmer dig/deen der åbner siden, for rigigt at indikere at "de" nu kan få alle dine oplysninger. 

derudover har jeg lavet en class i denne mini x, for at gøre det nemmere at hente oplysninger ind. 

### Hvad har du lært i denne miniX? Hvordan kunne du inkorporere eller videreudvikle koncepterne i din ReadMe/RunMe?
Jeg havde lært hvordan man laver classe, hvilket har gjort det nemmerer for mig at få min datat til at komme ind i mit program som jeg gerne ville have det til at komme ind.
Jeg har også lært hvordan man får sat billeder ind, både i forhold til bagrunden af den første side man kommer in dpå når man åbner programmet. Men ogå i forhold til at jeg har fået kamera til at virke, så den kan tage billeder/vise en på kameareret når man kommer ind på min andne side. 
Derudover synes jeg bare jeg generelt har lært både visual studio code, p5js og kodning bedre at kende, og dermed en bedre forståelse for hvordan man får tingene sat pænt op, og hvordan man får tingene til at fungere. 


### Hvad er relationen mellem æstetisk programmering og digital kultur?
De her to begreber hænger lidt sammen allerede synes jeg. Ligesom der i vores primære bog (soon & cox) også står skrevet om så kan man benytte æstetisk programmering til at bygge videre på det digitale vi kender allerede, altså den digitale kultur vi ser i dag. Man kan sige at at æstetisk programmering er med til at lave digital kultur, da det programmering skaber ting indenfor den digitale kultur, og hele tiden udvikler på den. 

_"Data is the “material produced by abstracting the world into categories, measures and other representational forms ...that constitute the building blocks from which information and knowledge are created” - Mejias et al (2015)_  

Jeg synes det her citat passer godt på data capture, da beskriver lidt om hvad data er. Aktså at data på en måde er noget menneskeksab, altså at det er menenkser der har valgt hvad der skal lægges betydning til ved at dele det op i nogle kategorier, for hvad vi mener der er vigtigt eller ej som data. 
