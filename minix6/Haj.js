class Haj {
  constructor() {
      this.posX = random(0, width);
      this.posY = random(75, height-65);
      this.speed = random(0.2, 2);
  }

  move(){
      this.posX += this.speed;
      if (this.posX > width){
          this.posX = 0;
      }
  }

  show() {
    //haj
    noStroke();
    fill(130, 16, 23);
    //hale
    triangle(this.posX-50, this.posY-15,this.posX-20,this.posY-13,this.posX-12, this.posY+10);
    triangle(this.posX-50, this.posY+10,this.posX-20,this.posY-13,this.posX-12, this.posY+10);
    //finne 
    triangle(this.posX+20, this.posY,this.posX-10,this.posY-30,this.posX-10, this.posY);
    //krop
      ellipse(this.posX, this.posY, 70, 30);
    //øje
    fill(0)
      arc(this.posX+20,this.posY-40 + 35,15,15,0,PI + QUARTER_PI,CHORD);
      fill(255);
      arc(this.posX+20,this.posY-40 + 35,5,5,0,PI + QUARTER_PI,CHORD);
      
      
      
      
  }
}


