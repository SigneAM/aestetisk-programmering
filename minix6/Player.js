class Player {
  constructor(){
      this.posX = width/2;
      this.posY = height-50;
      this.speed = 20;
      this.size = 50;
      
  }

  moveUp(){
      this.posY -= this.speed;
  }
  moveDown(){
      this.posY += this.speed;
  }
  moveRight(){
    this.posX += this.speed;
  }
  moveLeft(){
    this.posX -= this.speed;
  }
  
// ansigt til personen der skal svømme igennem 
  show(){
    //ansigts form 
      fill(255);
      stroke(0);
      fill(230,198,131);
      rect(this.posX, this.posY, this.size,this.size,21);
      //øjne 
      fill(194,234,242);
      ellipse(this.posX -8, this.posY + -8, 12,12);
      ellipse(this.posX + 10, this.posY + -8, 12,12);
      fill(0);
      ellipse(this.posX  -8, this.posY -8, 5,4);
      ellipse(this.posX +10, this.posY -8, 5,4);
      //mund
      arc(this.posX,this.posY-25 + 35,20,20,0,PI,CHORD);
      //snorkel 






  }
}


