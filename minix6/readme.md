# Mini x 6 - Game 
![](fisk.png)

[Se programmeringen her](https://SigneAM.gitlab.io/aestetisk-programmering/minix6/index.html) 

https://SigneAM.gitlab.io/aestetisk-programmering/minix6/index.html 
- kan ikke få koden til at virke, men kan ikke se hvad der er galt, så her kan i lige se linket.. 

[Se koden her til spil her](https://gitlab.com/SigneAM/aestetisk-programmering/-/blob/main/minix6/Hajspil.js)

[Se kode til player her](https://gitlab.com/SigneAM/aestetisk-programmering/-/blob/main/minix6/Player.js)

[Se koden til haj ](https://gitlab.com/SigneAM/aestetisk-programmering/-/blob/main/minix6/Haj.js)


### Describe how does/do your game/game objects work?
Jeg har (som man nok kan se) lavet spillet udfra det tomat spil vi lavede i klassen. Jeg havde svært ved at finde på noget nyt, og synes ikke en uge er så lang tid til at finde på noget nyt, og især så stor en kode som et spil er. 

Mit spil handler om at man er en figur/person/hovede der skal svømme forbi nogle piratfisk/hajer for at komme i mål og overleve. Man styrer figuren rundt på skørmen ved hjælp af piletasteren, og man kan både rykke op/ned og højre/venstre, så man kan bruge hele skærmen til at styrer uden om hajerne/fiskene. 

Jeg har lavet 2 classes, en for hajerne/fiskene og en for playeren og så har jeg selve spillet. 

Derudover har jeg lavet en class for goal, fordi det var min plan at man skulle kunne komme i mål, når man kom op i toppen af spillet og forbi alle fiskene. 
jeg prøvede at forsøge at gøre det ved at benytte `if statements`, og `for loop`, ligesom den måde man dør på hvis man rammer fiskene/hajerne Det har jeg dog ikke kunne få til at fungere, så det er noget jeg ville arbejde med hvis jeg skule forbedre denne mini x. 


### Describe how you program the objects and their related attributes, and the methods in your game.

Som jeg også beskriver ovenover så har jeg for at lave mit program og de forskellige objekter der er i programmet benyttet mig af classes. 
En class for player, hvor jeg har beskrevet hvordan player skal se ud, men også hvordan den skal bevæge sig. 
Det har jeg gjort udfra nogle contructors, altså jeg har lavet nogle paraamtre for at de vil se ud som den gør, lige meget hvor på skærmen den befinder sig.

``` 
  constructor(){
      this.posX = width/2;
      this.posY = height-50;
      this.speed = 20;
      this.size = 50;
```
Og får at kunne få min player til at bevæge sig op/ned og venstre/højre har jeg gjort således: 

``` 
   moveUp(){
      this.posY -= this.speed;
  }
  moveDown(){
      this.posY += this.speed;
  }
  moveRight(){
    this.posX += this.speed;
  }
  moveLeft(){
    this.posX -= this.speed;
```
Derudover har jeg en class for min fisk/hajer, som er contrueret meget på samme måde, dog er bævefelse for dem en smule anderledes og sat op som man kan se herunder.
Sådan så de kun kommer henover skærmen fra venstre til højre. 
Derudovr er deres `this.posX` sat til random højde, så de kan komme ind på alle punkter af skærmen. 

``` 
  move(){
      this.posX += this.speed;
      if (this.posX > width){
          this.posX = 0;
```
Jeg har så i min hovedfil, altså hajspillet filen/kodet, hentet de forskellige classes ind, så de bliver anvendt i spillet. 

Inde i min html fil, har jeg så linket til alle mine koder, alså både min `haj/fisk class`,`player class` og selve `hajspillet`, for at få alle til at køre på samme tid. 

### Connect your game project to a wider cultural context, and think of an example to describe how complex details and operations are being “abstracted”?

Jeg synes det er lidt svært at tage det i større kulturel kontekst. Men jeg kan sankke lidt udfra min ide til spillet, jeg har først og frememst benyttet mig af det tomat spil vi udviklede i klassen. Men jeg har derudover tænkt over andre spil der kan minde om det, som fx. crossy roads, som mange kender til. Jeg ville derfor gerne lave et spil som mange kebdte til, og dermed vidste hvordan skulle opereres. Derudover har jeg valgt at lave de objekter man skal undgå ti noget der giver mening der skal undgåes, alytså disse hajer/piratfisk. Dette er noget som jeg tror alle ville prøve at undgå hvis man selv var ude og svømme, og jeg føler derfor det giver mening at vælge noget der symbolisere dem. 

Umiddelbart er mit spil meget genkendligt og simpel, da det er et spil som jeg nævner før også, er det mange allerede kender til bare i et lidt andet format, eller med et lidt andet udseende. Dette taler meget godt til mange mennekser, for mange kan godt lide hvis man kan genkende noget nyt i noget man allerede kender. Så selvom det kan virke meget simpelt, kan det være godt for at appellere til mange mennesker. 


